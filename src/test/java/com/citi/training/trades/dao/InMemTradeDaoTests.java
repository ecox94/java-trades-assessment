package com.citi.training.trades.dao;

import static org.junit.Assert.*;

 
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

 

public class InMemTradeDaoTests {

	private static Logger LOG = LoggerFactory.getLogger(InMemTradeDao.class);

 	private String testStock = "AAPL";
	private double testPrice = 500.99;
	private int testVolume = 50;



	
    @Test
    public void test_saveAndGetEmployee() {
        Trade testTrade = new Trade(-1, testStock, testPrice, testVolume);
        InMemTradeDao testRepo = new InMemTradeDao();

        testTrade = testRepo.create(testTrade);

        assertTrue(testRepo.findById(testTrade.getId()).equals(testTrade));

    }



     

    @Test(expected = TradeNotFoundException.class)
    public void test_deleteTrade() {
        Trade[] testTradeArray = new Trade[100];
        InMemTradeDao testRepo = new InMemTradeDao();

        for (int i = 0; i < testTradeArray.length; ++i) {
        	testTradeArray[i] = new Trade(-1, testStock, testPrice, testVolume);

            testRepo.create(testTradeArray[i]);
        }
        Trade removedTrade = testTradeArray[10];
        testRepo.deleteById(removedTrade.getId());
        LOG.info("Removed item from Trade array");
        testRepo.findById(removedTrade.getId());
    }

    @Test 
   (expected = TradeNotFoundException.class)
    public void test_deleteNotFoundEmployee() {
        InMemTradeDao testRepo = new InMemTradeDao();
        testRepo.create(new Trade(2, testStock, testPrice, testVolume));
        testRepo.deleteById(99);
    }

}
