package com.citi.training.trades.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;



 
import com.citi.training.trades.model.Trade;






@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	@Autowired
    MysqlTradeDao  mysqlTradeDao;

    @Test
    @Transactional
    public void test_findByID() {
    Trade testTrade	= mysqlTradeDao.create(new Trade(-1, "AAPL", 500.99, 50));
    	
     	
    	assertEquals(testTrade, mysqlTradeDao.findById(-1));
     }



}
