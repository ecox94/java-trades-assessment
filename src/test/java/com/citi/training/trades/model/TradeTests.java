package com.citi.training.trades.model;

import static org.junit.Assert.*;

import org.junit.Test;

 
public class TradeTests {

	private int testID = 94;
	private String testStock = "AAPL";
	private double testPrice = 500.99;
	private int testVolume = 50;

	@Test
	public void testConstructor() {
		Trade testTrade = new Trade(testID, testStock, testPrice, testVolume);
	
		assertEquals(testID, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice, testTrade.getPrice(), 0.005);
		assertEquals(testVolume, testTrade.getVolume());

	}
	
	
	@Test
	public void testToString() {
		String testString = ( new Trade(testID, testStock, testPrice, testVolume).toString());
		assertTrue (testString.contains((new Integer(testID)).toString()));
		assertTrue(testString.contains(testStock));
		assertTrue (testString.contains((new Double(testPrice)).toString()));
		assertTrue (testString.contains((new Integer(testVolume)).toString()));


		
	}

}
