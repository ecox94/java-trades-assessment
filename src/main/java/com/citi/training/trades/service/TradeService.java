package com.citi.training.trades.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;

/**
 * Service Layer class
 * @author Administrator
 *
 */
public class TradeService {
	@Autowired
	private TradeDao tradeDao;

	public Trade findById(int id) {
		return tradeDao.findById(id);
	}

	public Trade create(Trade trade) {
		if (trade.getStock().length() > 0) {
			return tradeDao.create(trade);

		}
		throw new RuntimeException("Invalid Parameter: trade name: " + trade.getStock());
	}

	public void deleteById(int id) {
		tradeDao.deleteById(id);
	}

}
