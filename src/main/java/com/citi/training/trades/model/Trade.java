package com.citi.training.trades.model;

public class Trade {

	private int id;
	private String stock;
	private double price;
	private int volume;

	/**
	 * Default Constructor
	 */
	public Trade() {

	}

	/**
	 * Constructor with args
	 * @param id
	 * @param stock
	 * @param price
	 * @param volume
	 */
	public Trade(int id, String stock, double price, int volume) {
		this.id= id;
		this.stock = stock;
		this.price = price;
		this.volume = volume;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	
	 /**
	  * Generates a database row to a String
	  */
	public String toString() {
		// TODO Auto-generated method stub
		return "Trade id: " + id + " Stock Symbol : " +  stock + " Price : $" + price + " Volumes " + volume;
		
	}

	
}
