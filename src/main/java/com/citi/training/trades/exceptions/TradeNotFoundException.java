package com.citi.training.trades.exceptions;

@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {
	
	/**
	 * 
	 * Exception for when an attempt to delete or find a 
	 * {@link com.citi.training.trades.model.Trade} does not exist
	 * @param msg
	 */
	public TradeNotFoundException(String msg) {
		super(msg);
	}

}
