package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trade;

 /**
  * Interface implemented by:
  * {@link com.citi.training.trades.dao.inMemTradeDao}
  * {@link com.citi.training.trades.dao.MysqlTradeDao}
  * 
  * @author Administrator
  *
  */
public interface TradeDao {

 
	  Trade findById(int id);

	  Trade create(Trade trade);

	   void deleteById(int id);

}
